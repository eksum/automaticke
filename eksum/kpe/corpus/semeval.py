'''

@author: sumukh
'''
import re
import os.path

class SemEval2010:
    
    def __init__(self, root, select=set(['train', 'trial'])):
        self.root = root
        print('\tSelected corpus section(s): ' + ', '.join(select))
        self.select = select
        if 'train' in self.select:
            self.train = SEFolder(self.subfolder('train'))
        if 'test' in self.select:
            self.test = SEFolder(self.subfolder('test'))
        if 'trial' in self.select:
            self.trial = SEFolder(self.subfolder('trial'))
        
    def subfolder(self, foldername):
        return self.root + os.path.sep + foldername
    
    def iter_docs(self):
        if 'train' in self.select:
            for train_doc in self.train.iter_docs():
                yield train_doc
        if 'test' in self.select:
            for test_doc in self.test.iter_docs():
                yield test_doc
        if 'trial' in self.select:        
            for trial_doc in self.trial.iter_docs():
                yield trial_doc
    

class SEFolder:
    TF_EXT = '.txt.final'
    
    def __init__(self, path):
        self.foldername = os.path.basename(path)
        files = os.listdir(path)
        self.text_files = dict([ (fil[:len(SEFolder.TF_EXT) * -1], SETextFile(path + os.path.sep + fil)) for fil in files if os.path.isfile(path + os.path.sep + fil) and fil.endswith(SEFolder.TF_EXT)])
        self.author_anno = self.get_anno_file(path, self.foldername, 'author', False)
        self.combined_anno = self.get_anno_file(path, self.foldername, 'combined', False)
        self.reader_anno = self.get_anno_file(path, self.foldername, 'reader', False)
        self.author_anno_stem = self.get_anno_file(path, self.foldername, 'author', True)
        self.combined_anno_stem = self.get_anno_file(path, self.foldername, 'combined', True)
        self.reader_anno_stem = self.get_anno_file(path, self.foldername, 'reader', True)
 
    def get_anno_file(self, path, foldername, anno_by, is_stem):
        full_path = path + os.path.sep + foldername + '.' + anno_by
        if is_stem:
            full_path += '.stem'
        full_path += '.final'
        anno_file = SEAnnoFile(full_path)
        anno_file.read()
        return anno_file
    
    def iter_docs(self):
        for key in self.text_files.iterkeys():
            item = {}
            item['id'] = self.foldername + '.' + key
            txt_file = self.text_files[key]
            all_text = txt_file.get_text()
            item['text'] = all_text
            abstract = txt_file.get_abstract(all_text)
            item['abs'] = abstract
            sec1 = txt_file.get_introduction(all_text)
            sec2 = txt_file.get_background(all_text)
            item['abs-sec-1-2'] = ' '.join([abstract, sec1, sec2]).strip()
            sec3 = txt_file.get_section(all_text, 3)
            sec4 = txt_file.get_section(all_text, 4)
            item['abs-sec-1-2-3-4'] = ' '.join([abstract, sec1, sec2, sec3, sec4]).strip()
            item['without-references'] = txt_file.all_except_references(all_text)
            item['annos'] = {}
            item['annos']['author'] = self.author_anno[key]
            item['annos']['combined'] = self.combined_anno[key]
            item['annos']['reader'] = self.reader_anno[key]
            item['annos']['author-stem'] = self.author_anno_stem[key]
            item['annos']['combined-stem'] = self.combined_anno_stem[key]
            item['annos']['reader-stem'] = self.reader_anno_stem[key] 
            yield item       
        
        
class SEAnnoFile(dict):
    
    def __init__(self, filename):
        self.filename = filename
        
    def read(self):
        with open(self.filename, 'r') as f:
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    continue
                parts = line.split(':')
                self[parts[0].strip()] = set(parts[1].strip().split(','))
        
class SETextFile:
    ABSTRACT_TITLE = re.compile(r'^[\s]*ABSTRACT[\s]*$', flags= re.I | re.M)
    REFERENCES_TITLE = re.compile(r'^(\d+\\.\s*)?REFERENCES[\s]*$', flags=re.I | re.M)
    CATEGORIES_TITLE = re.compile(r'^[\s]*Categories and Subject Descriptors[\s]*$', flags = re.I | re.M)
    
    def __init__(self, filename):
        self.filename = filename
        
    def get_text(self):
        lines = []
        with open(self.filename, 'r') as f:
            for line in f:
                lines.append(line)
        return '\n'.join(lines)
    
    def get_abstract(self, text):
        abstract_match = SETextFile.ABSTRACT_TITLE.search(text)
        if abstract_match is not None:
            remaining = text[abstract_match.end():]
            categories_match = SETextFile.CATEGORIES_TITLE.search(remaining)
            if categories_match is not None:
                return remaining[:categories_match.start()].strip()
        return ''
    
    def get_introduction(self, text):
        return self.get_section(text, 1)
        
    def get_section(self, text, num):
        title_regex = re.compile(r'^' + str(num) + '\\.[\s]?[A-Z]+[\s]*$', flags=re.M)
        next_title_regex = re.compile(r'^' + str(num + 1) + '\\.[\s]?[A-Z]+[\s]*$', flags=re.M)
        match = title_regex.search(text)
        if match is not None:
            remaining = text[match.end():]
            next_match = next_title_regex.search(remaining)
            if next_match is not None:
                return remaining[:next_match.end()].strip()
        return ''
    
    def get_background(self, text):
        return self.get_section(text, 2)
    
    def all_except_references(self, text):
        references_match = SETextFile.REFERENCES_TITLE.search(text)
        if references_match is not None:
            return text[:references_match.start()].strip()
        return text
            