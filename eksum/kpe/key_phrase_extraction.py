'''
@author: sumukh
'''
import re
import nltk
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.stem import porter
from sklearn.feature_extraction.text import TfidfVectorizer
from operator import mul
from operator import add
from operator import itemgetter
import pdb

class AutomaticKeyPhraseExtraction:
    def __init__(self, log_errors=False):
        self.alnum_regex = re.compile('\w')
        self.log_errors = log_errors
    
    def initialize(self, corpus, text_selection='text', stem=False):
        self.texts = {}
        self.annos = {}
        self.stemmer = None
        if stem:
            self.stemmer = porter.PorterStemmer()
        num_docs = 0
        print('Initializing corpus...')
        if text_selection not in set(['text', 'abs', 'abs-sec-1-2', 'abs-sec-1-2-3-4', 'without-references']):
            print('Warning: Unknown text_selection setting ' + text_selection + "; resetting to 'text'")
            text_selection = 'text'
        print('\tText selection setting: ' + text_selection)
        print('\tUse stemming: ' + str(stem))

        for doc in corpus.iter_docs():
            doc_id = doc['id']
            text = doc[text_selection]
            if len(text) == 0:
                continue
            sents = sent_tokenize(text.decode('utf-8'))
            self.texts[doc_id] = sents
            self.annos[doc_id] = doc['annos']
            num_docs += 1
            # print(id + ' ' + str(len(kps)))
        print('\tNumber of docs in corpus: ' + str(num_docs))
            
    def generate_candidate_kps_by_id(self):
        grammar = r'''
            KEYPHRASE: 
                {<JJ.*>?(<NN.*>|<VBG>)*<NN.*>}
                {<JJ.*>?<NN.*>+}
        '''
        chunker = nltk.RegexpParser(grammar)

        kps_by_id = {}
        print('\nExtracting candidate phrases...')
        for text_id in self.texts.iterkeys():
            sents = self.texts[text_id]
            kps = []
            for sent in sents:
                tokens = word_tokenize(sent)
                pos_tokens = pos_tag(tokens)
                tree = chunker.parse(pos_tokens)
                kps.extend([kp for kp in self.key_phrases_in_tree(tree)])
            kps_by_id[text_id] = ' '.join(kps)
        return kps_by_id
    
    def expected_kp_stats(self, anno_by):
        print('\nExpected key phrase stats...')
        expected_kps_by_id = dict([(doc_id, doc_annos[anno_by]) for doc_id, doc_annos in self.annos.items()])
        self.kp_stats(expected_kps_by_id)
    
    def kp_stats(self, kps_by_id):
        total = 0
        for k,v in kps_by_id.items():
            total += len(v)
        print('\tTotal num of key phrases: ' + str(total))
        print('\tAverage num of key phrases per doc: ' + str(total * 1.0 / len(kps_by_id)))
                    
    '''
    This function evaluates the performance of the tfidf ranking by varying tfidf threshold between start_tfidf and end_tfidf
    '''
    def evaluate_kp_ranking(self, docs_by_id, anno_by, norm='l2', start_tfidf=None, end_tfidf=None, num_incrs=20, top_n=None):
        if self.stemmer is not None and not anno_by.endswith('-stem'):
            anno_by += '-stem'
        print('\tAnnotation reference: ' + anno_by)
        print('\tNum of top_n key phrases: ' + str(top_n))
        print('\tNorm: ' + str(norm))
        ids = docs_by_id.keys()
        kp_docs = [docs_by_id[idx] for idx in ids]
        
        if self.log_errors:
            expected_not_present = []
            present_not_expected = []
        
        tv = TfidfVectorizer(norm=norm, min_df=1)
        doc_term_matrix = tv.fit_transform(kp_docs)
        
        if start_tfidf is None:
            start_tfidf = min(doc_term_matrix.data)
        if end_tfidf is None:
            end_tfidf = max(doc_term_matrix.data)
        
        print('\tStart tfidf: ' + str(start_tfidf))
        print('\tEnd tfidf: ' + str(end_tfidf))
        incrs = map(mul, [(end_tfidf - start_tfidf) * 1.0 / num_incrs] * num_incrs, range(num_incrs))
        thresholds = map(add, incrs, [start_tfidf] * num_incrs) 
            
        kps_by_idx = tv.get_feature_names()
        
        num_docs = doc_term_matrix.shape[0]
        print('\nThreshold\tPrecision\tRecall\tFscore\tAverage KPS/doc')    
        for threshold in thresholds:
            total_returned = 0 
            total_precision = 0
            total_recall = 0
            
            for idx in range(num_docs):
                returned_kps = self.get_kps_for_doc(doc_term_matrix[idx], kps_by_idx, threshold, top_n)
                total_returned += len(returned_kps)
                doc_id = ids[idx]    
                expected_kps = self.annos[doc_id][anno_by]
                precision, recall = self.get_precision_recall(expected_kps, returned_kps)
                total_precision += precision
                total_recall += recall
                
                if self.log_errors:
                    expected_not_present.append(doc_id + ' [' + str(threshold) + ']' +' : ' + str(expected_kps.difference(returned_kps)))
                    present_not_expected.append(doc_id + ' [' + str(threshold) + ']' + ' : ' + str(returned_kps.difference(expected_kps)))
                
            
            avg_precision = total_precision / num_docs
            avg_recall = total_recall / num_docs
            prec_recall_sum = avg_precision + avg_recall
            if prec_recall_sum == 0.0:
                fscore = 0
            else:
                fscore = 2.0 * avg_precision * avg_recall / prec_recall_sum 

            print('{0:.5g}'.format(threshold) + '\t' + str(avg_precision) + '\t' + str(avg_recall) + '\t' + str(fscore) + '\t' + str(total_returned * 1.0 / num_docs))
        
        if self.log_errors:
            ebnp = open('ebnp' + str(start_tfidf) + '-' + str(end_tfidf), 'w')
            ebnp.write('\n'.join(expected_not_present) + '\n')
            ebnp.close()
        
            pbne = open('pbne' + str(start_tfidf) + '-' + str(end_tfidf), 'w')
            pbne.write('\n'.join(present_not_expected) + '\n')
            pbne.close()
            
    def get_kps_for_doc(self, term_matrix, kps_by_idx, threshold, top_n):
        coord_form = term_matrix.tocoo()
        term_tfidf_tuples = zip(coord_form.col, coord_form.data)
        
        kps_scores = set([])
        for term_idx, tfidf in term_tfidf_tuples:
            kp = kps_by_idx[term_idx].replace('__', ' ').lower().strip()
            if self.is_valid_kp(kp, tfidf, threshold):
                kps_scores.add((kp, tfidf))
        kps_scores_list = list(kps_scores)
        kps_scores_list.sort(key=itemgetter(1), reverse=True)
        kps = [ k for k,s in kps_scores_list ]
        #pdb.set_trace()
        before_length = len(kps)
        if top_n is not None:
            kps = kps[:top_n]
            after_length = len(kps)
        return set(kps)
        
    def is_valid_kp(self, kp, tfidf, threshold):
        return tfidf >= threshold and not kp.isdigit() and self.has_min_length(kp)
    
    def has_min_length(self, kp):
        num_words = kp.count(' ') + 1
        if num_words == 1:
            return len(kp) > 2
        if num_words == 2:
            return len(kp) > 5
        return len(kp) > 8
                            
    def get_precision_recall(self, expected_kp, key_phrases):
        intersection_size = len(key_phrases.intersection(expected_kp)) * 1.0
        
        if len(key_phrases) > 0:
            precision = intersection_size / len(key_phrases)
        else:
            precision = 0
        if len(expected_kp) > 0:
            recall = intersection_size / len(expected_kp)
        else:
            recall = 1.0
        return (precision, recall)


    def non_alnum_word(self, word):
        return len(self.alnum_regex.findall(word)) == 0
    
    def key_phrases_in_tree(self, tree):
        for subtree in tree.subtrees(filter=lambda t: t.label() == 'KEYPHRASE'):
            leaves = subtree.leaves()
            kp_words = [w.lower() for w, t in leaves if not self.non_alnum_word(w)]                
            # len(kp_words) == 0 should ideally not happen, unless some symbols are wrongly tagged
            # len(kp_words) > 5 is likely to be a list of headings or a table, so ignore
            if len(kp_words) == 0 or len(kp_words) > 5:
                continue
            if self.stemmer is not None:
                kp_words = [self.stemmer.stem(word) for word in kp_words]
            yield '__'.join(kp_words)
            
