'''
@author: sumukh
'''
import unittest
from eksum.kpe.corpus import semeval

class SemEvalTest(unittest.TestCase):
    
    def test_annotation_file(self):
        seaf = semeval.SEAnnoFile('data/SemEval2010/train/train.author.final')
        seaf.read()
        self.assertEqual(144, len(seaf))
        values = seaf['C-42']
        expected = set(['reservoir model','energy exploration','enkf','tigre'])
        self.assertEqual(len(values), len(expected), 'Incorrect number of expected elements')
        nullset = expected.difference(values)
        self.assertEqual(0, len(nullset))
        
    def test_folder(self):
        folder = semeval.SEFolder('data/SemEval2010/train')
        self.assertEqual(147, len(folder.text_files))