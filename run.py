'''

@author: sumukh
'''
from os.path import sep
from eksum.kpe.corpus.semeval import SemEval2010
from eksum.kpe.key_phrase_extraction import AutomaticKeyPhraseExtraction

#corpus = SemEval2010('.' + sep + 'data' + sep + 'SemEval2010')
corpus = SemEval2010('/Users/sumukh/Develop/workspace/AutomaticKE/data/SemEval2010', select=set(['train']))
akpe = AutomaticKeyPhraseExtraction()
akpe.initialize(corpus, 'text', True)

anno_by = 'combined'
akpe.expected_kp_stats(anno_by)
candidate_key_phrases = akpe.generate_candidate_kps_by_id()
akpe.kp_stats(candidate_key_phrases)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; no top')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 5')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=5)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 10')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=10)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 15')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=15)

print('\nEvaluating performance of ' + anno_by + ' l2 norm; end_tfidf=0.08; top 5')
akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, end_tfidf=0.08, top_n=5)

print('\nEvaluating performance of ' + anno_by + ' l2 norm; end_tfidf=0.08; top 10')
akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, end_tfidf=0.08, top_n=10)

print('\nEvaluating performance of ' + anno_by + ' l2 norm; end_tfidf=0.08; top 15')
akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, end_tfidf=0.08, top_n=15)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 20')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=20)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 30')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=30)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 50')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=50)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; default start-stop; top 75')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, top_n=75)

#print('\nEvaluating performance of ' + anno_by + ' l1 norm; default start-stop; no top')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm='l1')

#print('\nEvaluating performance of ' + anno_by + ' l1 norm; default start-stop; top 15')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm='l1', top=15)

#print('\nEvaluating performance of ' + anno_by + ' l1 norm; default start-stop; top 20')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm='l1', top=20)

#print('\nEvaluating performance of ' + anno_by + ' l1 norm; default start-stop; top 30')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm='l1', top=30)

#print('\nEvaluating performance of ' + anno_by + ' no norm; default start-stop; no top')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm=None)

#print('\nEvaluating performance of ' + anno_by + ' no norm; default start-stop; top 15')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm=None, top=15)

#print('\nEvaluating performance of ' + anno_by + ' no norm; default start-stop; top 20')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm=None, top=20)

#print('\nEvaluating performance of ' + anno_by + ' no norm; default start-stop; top 30')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, norm=None, top=30)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; start=0.24; end=0.33; no top')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, start_tfidf=0.24, end_tfidf=0.33)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; start=0.24; end=0.33; top 15')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, start_tfidf=0.24, end_tfidf=0.33, top=15)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; start=0.24; end=0.33; top 20')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, start_tfidf=0.24, end_tfidf=0.33, top=20)

#print('\nEvaluating performance of ' + anno_by + ' l2 norm; start=0.24; end=0.33; top 30')
#akpe.evaluate_kp_ranking(candidate_key_phrases, anno_by, start_tfidf=0.24, end_tfidf=0.33, top=30)
